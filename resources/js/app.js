require('./bootstrap');

// Require Vue Components
window.Vue = require('vue').default;

// Register Vue Components
Vue.component('example-component', 
require('./components/ExampleComponent.vue').default);

// Init Vue
const app = new Vue({
    el: '#app'
})